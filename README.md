[![](https://framasoft.org/nav/img/icons/ati/sites/git.png)](https://framagit.org)

🇬🇧 **Framasoft uses GitLab** for the development of its free softwares. Our GitHub repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your GitHub account)

🇫🇷 **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts GitHub ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l’inscription n’est pas nécessaire, vous pouvez vous connecter avec votre compte GitHub)
* * *

Architecture de base pour un site statique réactif et internationalisé à l’aide de :
- Vuejs 2
- Vue-i18n + Vue-router pour l’internationnalisation des pages
- Import de Bootstrap et IconsLK
- YAML pour les fichiers de langue (plus lisible qu’un JSON mais automatiquement converti)
- SASS pour l’habillage
- Webpack 4 pour automatiser la construction de l’ensemble

## En prod
Pour construire le site :

```
npm run commons
npm run prod
```

Les fichiers sont placés dans le dossier `dist`.
Le site fonctionne à la racine du domaine
ou dans un sous-dossier sur les Gitlab Pages (correspondant au nom du dépôt).

Les pages sont prérendues **avec les traductions dans le code html** en prod
et **sans** sur Gitlab Pages (la `fallbackLocale` est utilisée ;
les traductions sont chargées dynamiquement).

## En développement
Pour voir le site en local

```
npm run commons
npm run dev
```

Les changements s’appliquent en temps réel et se voient sur http://localhost:8080/.

```
├── config                    # fichiers de config pour webpack selon l’environnement (dev, preview, prod)
│
├── commons                   # fichiers communs à l’ensemble des sites de Framasoft (même arborescence que `src`)
│
├── dist                      # fichiers générés pour la prod/preview
│
├── public                    # fichiers copiés dans `dist` pour être servis directement
│   ├── fonts                 # fonts spécifiques au projet
│   └── img
│       ├── en                # images contenant du texte à traduire (bd, infographie, etc)
│       ├── fr                # (à défaut, des liens symboliques vers l’équivalent dans la langue de fallback sont créés)
│       ├── icons             # favicon, apple-touch-icons…
│       └── opengraph         # twitter, oembed…
│
├── src
│   │
│   ├── components            # composants de structure ou qui se répètent
│   │   └── Header.vue        # comme un en-tête ou menu de navigation
│   │
│   ├── data
│   │   └── main.yml          # données du site qui ne requièrent pas de traduction
│   │
│   ├── scss
│   │   ├── fonts             # fonts importées comme dépendance (DejaVu, Roboto…)
│   │   ├── frama             # charte graphique Frama pour Bootstrap 3 et 4
│   │   ├── frameworks        # composants tiers Bootstrap, IconsLK, etc
│   │   └── main.scss         # le fichier compilé est minifié dans `/public/style.css`
│   │
│   ├── translations          # traductions
│   │   ├── en
│   │   │   ├── main.yml      # traduction principale du site
│   │   │   └── example.yml   # traduction d’une page, d’une section, etc…
│   │   └── fr
│   │       ├── main.yml
│   │       └── example.yml
│   │
│   ├── views
│   │   ├── Example.vue       # page d’example
│   │   └── Home.vue          # page d’accueil racine du site
│   │
│   ├── App.vue               # template vue qui définie le cadre général
│   ├── data.js               # moulinette d’import des données communes et spécifiques au site
│   ├── i18n.js               # gestion de l’i18n
│   ├── main.js               # gestion de l’i18n + routage des pages + import des assets
│   └── router.js             # routage des pages
│
├── index.html                # le fichier est simplement copié dans `/public`
├── package.json              # liste des dépendances + définition de commandes npm run dev|prod
└── webpack.config.js         # config du site, activation de la nav, etc
```
